import React from "react";

import { Header } from "components/Header";

import { useStyles } from "./styles";

export const PageLayout: React.FC = props => {
  const classes = useStyles();

  return (
    <>
      <Header />
      <main className={classes.main}>
        {props.children}
      </main>
    </>
  );
};
