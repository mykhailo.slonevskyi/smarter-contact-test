// Firebase App (the core Firebase SDK) is always required and must be listed first
import firebase from "firebase/app";

// Add the Firebase products that you want to use
import "firebase/firestore";

import {
  FB_APP_ID,
  FB_API_KEY,
  FB_PROJECT_ID,
  FB_AUTH_DOMAIN,
  FB_STORAGE_BUCKET,
  FB_MEASUREMENT_ID,
  FB_MESSAGING_SENDER_ID,
} from "constants/config";

const firebaseConfig = {
  apiKey: FB_API_KEY,
  authDomain: FB_AUTH_DOMAIN,
  projectId: FB_PROJECT_ID,
  storageBucket: FB_STORAGE_BUCKET,
  messagingSenderId: FB_MESSAGING_SENDER_ID,
  appId: FB_APP_ID,
  measurementId: FB_MEASUREMENT_ID
};

export const FB = firebase.initializeApp(firebaseConfig)
