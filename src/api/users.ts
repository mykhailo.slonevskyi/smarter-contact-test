import { FB } from "services/firebase";

import { USERS } from "constants/strings";

const db = FB.firestore();

export const getUsersApi = () => db.collection(USERS).get()
  .then((querySnapshot) => {
    const users = [] as any;

    querySnapshot.forEach(doc => {
      users.push({
        id: doc.id, // TODO find better way for create and working with id
        ...doc.data()
      })
    })

    return users
  });

export const addUserApi = user => db.collection(USERS).add({
  ...user
}).then(() => alert("User added successfully!"))

//TODO change request, add id and use spread operator
export const editUserApi = user => db.collection(USERS).doc(user.id).set({
  name: user.name,
  surname: user.surname,
  email: user.email
}, { merge: true }).then(() => alert("User edited successfully!"))
