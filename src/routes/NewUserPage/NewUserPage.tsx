import React from "react";

import { PageLayout } from "layouts/PageLayout";

import { NewUserForm } from "components/NewUserForm";

export const NewUserPage: React.FC = () => (
  <PageLayout>
    <NewUserForm />
  </PageLayout>
)
