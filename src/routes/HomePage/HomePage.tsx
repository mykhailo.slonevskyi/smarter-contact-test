import React from "react";

import { PageLayout } from "layouts/PageLayout";

import { UsersTable } from "components/UsersTable";

export const HomePage: React.FC = () => (
  <PageLayout>
    <UsersTable />
  </PageLayout>
)
