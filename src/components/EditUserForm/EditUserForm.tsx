import React from "react";
import { Controller, useForm } from "react-hook-form";

import { Box } from "UI/Box";
import { Button } from "UI/Button";
import { TextField } from "UI/TextField";
import { DialogContent } from "UI/DialogContent";
import { DialogActions } from "UI/DialogActions";

import { useUsersContext } from "components/UsersTable/context";

import { editUserApi } from "api/users";

import { IEditUserFormProps } from "./types";

export const EditUserForm: React.FC<IEditUserFormProps> = props => {
  const { getUsers } = useUsersContext();
  const { handleSubmit, control } = useForm({
    defaultValues: props.user
  })

  const onSubmit = data => editUserApi(data)
    .then(() => getUsers())
    .then(() => props.handleCloseModal())

  const handleEditUserSubmit = () => {
    handleSubmit(onSubmit)()
  }

  return (
    <>
      <DialogContent>
        <form>
          <Controller
            name="name"
            control={control}
            render={({
                       field: { onChange, value },
                     }) => (
              <TextField
                label="Name"
                value={value}
                onChange={onChange}
              />
            )}
          />
          <Box mt={1}>
            <Controller
              name="surname"
              control={control}
              render={({
                         field: { onChange, value },
                       }) => (
                <TextField
                  label="Surname"
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </Box>
          <Box mt={1}>
            <Controller
              name="email"
              control={control}
              render={({
                         field: { onChange, value },
                       }) => (
                <TextField
                  label="Email"
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </Box>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.handleCloseModal}>
          Cancel
        </Button>
        <Button onClick={handleEditUserSubmit} color="primary">
          Edit
        </Button>
      </DialogActions>
    </>
  )
}
