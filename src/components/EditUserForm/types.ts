export interface IEditUserFormProps {
  user: IUser;
  handleCloseModal: () => void;
}