import React, { useEffect } from "react";

import { Box } from "UI/Box";
import { Progress } from "UI/Progress";
import { TableRow } from "UI/TableRow";
import { TableCell } from "UI/TableCell";
import { TableBody } from "UI/TableBody";

import { useUsersContext } from "./context";

import { UsersTableBodyRow } from "./UsersTableBodyRow";

export const UsersTableBody = () => {
  const { users, getUsers } = useUsersContext();

  useEffect(() => {
    getUsers();
  }, [])

  return (
    <TableBody>
      {users.length !== 0 && users.map((user, index) => (
        <UsersTableBodyRow
          key={JSON.stringify(user)} //TODO change JSON.stringify to id
          user={user}
          index={index}
        />
      ))}
      {users.length === 0 && (
        <TableRow>
          <TableCell colSpan={5}>
            <Box display="flex" justifyContent="center">
              <Progress />
            </Box>
          </TableCell>
        </TableRow>
      )}
    </TableBody>
  )
}
