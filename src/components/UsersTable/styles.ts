import { makeStyles, Theme } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme: Theme) => ({
  row: {
    padding: theme.spacing(1, 2)
  }
}));
