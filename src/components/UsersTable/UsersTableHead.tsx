import React from "react";

import { TableRow } from "UI/TableRow";
import { TableHead } from "UI/TableHead";
import { TableCell } from "UI/TableCell";

const getHeadCells = [
  { id: "name", label: "name" },
  { id: "surname", label: "surname" },
  { id: "email", label: "email" },
];

export const UsersTableHead: React.FC = () => (
  <TableHead>
    <TableRow>
      <TableCell align="justify" >
        #
      </TableCell>
      {getHeadCells.map(({ id, label }) => (
          <TableCell key={id} align="justify">
            {label}
          </TableCell>
      ))}
      <TableCell width={30} />
    </TableRow>
  </TableHead>
);
