import React from "react";

import { TableRow } from "UI/TableRow";
import { TableCell } from "UI/TableCell";

import { EditUserModal } from "components/EditUserModal";

import { IUsersTableBodyRowProps } from "./types";

export const UsersTableBodyRow: React.FC<IUsersTableBodyRowProps> = props => (
  <TableRow>
    <TableCell>
      {props.index}
    </TableCell>
    <TableCell>
      {props.user.name}
    </TableCell>
    <TableCell>
      {props.user.surname}
    </TableCell>
    <TableCell>
      {props.user.email}
    </TableCell>
    <TableCell>
       <EditUserModal user={props.user} />
    </TableCell>
  </TableRow>
)
