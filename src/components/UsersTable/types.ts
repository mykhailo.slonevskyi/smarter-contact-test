export interface IUsersTableBodyRowProps {
  index: number;
  user: IUser;
}

export interface IUsersContext {
  users: IUser[];
  getUsers: () => void;
}