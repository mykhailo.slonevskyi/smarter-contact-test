import { useState, useMemo } from "react";

import { getUsersApi } from "api/users";

function useContextHandler() {
  const [users, setUsers] = useState<IUser[]>([]);

  const getUsers = () => getUsersApi()
    .then(users => setUsers(users))

  const value = useMemo(() => ({
    users,
    getUsers,
  }), [users]);

  return ({
    value
  });
}

export {
  useContextHandler,
};
