import React from "react";

import { Table } from "UI/Table";
import { TableContainer } from "UI/TableContainer";

import { UsersTableHead } from "./UsersTableHead";
import { UsersTableBody } from "./UsersTableBody";

export const UsersTable: React.FC = () => {

  return (
    <TableContainer>
      <Table>
        <UsersTableHead />
        <UsersTableBody />
      </Table>
    </TableContainer>
  )
}
