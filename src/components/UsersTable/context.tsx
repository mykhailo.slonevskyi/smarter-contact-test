import React from "react";

import { useContextHandler } from "./useContextHandler";

import { IUsersContext } from "./types";

const UsersContext = React.createContext({} as IUsersContext);

const UsersProvider: React.FC = props => {
  const { value } = useContextHandler();

  return (
    <UsersContext.Provider value={value}>
      {props.children}
    </UsersContext.Provider>
  );
};

function useUsersContext() {
  const context = React.useContext(UsersContext);

  if (!context) {
    throw new Error("useUsersContext must be inside UsersProvider");
  }

  return context;
}

export {
  UsersProvider,
  useUsersContext,
};
