import React, { useState } from "react";
import PencilIcon from "mdi-material-ui/Pencil";

import { Dialog } from "UI/Dialog";
import { IconButton } from "UI/IconButton";
import { DialogTitle } from "UI/DialogTitle";

import { EditUserForm } from "components/EditUserForm";

import { IEditUserModalProps } from "./types";

export const EditUserModal: React.FC<IEditUserModalProps> = props => {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleCloseModal = () => {
    setOpen(false);
  };

  return (
    <div>
      <IconButton onClick={handleClickOpen}>
        <PencilIcon />
      </IconButton>
      <Dialog fullWidth open={open} onClose={handleCloseModal} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Edit user {props.user.name}</DialogTitle>
        <EditUserForm user={props.user} handleCloseModal={handleCloseModal} />
      </Dialog>
    </div>
  )
}