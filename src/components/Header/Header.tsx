import React from "react";

import { MainNav } from "components/MainNav";

export const Header: React.FC = () => (
  <header>
    <MainNav />
  </header>
)
