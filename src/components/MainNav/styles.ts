import { makeStyles, Theme } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme: Theme) => ({
  nav: {
    padding: theme.spacing(1, 2, 0),
    backgroundColor: theme.palette.background.default,
    borderBottom: `1px solid ${theme.palette.divider}`,
    marginBottom: 0 //fix material margin
  },
  activeText: {
    color: theme.palette.primary.main,
  }
}));
