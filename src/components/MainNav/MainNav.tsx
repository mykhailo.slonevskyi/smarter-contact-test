import React, { FC } from "react";

import { Grid } from "UI/Grid";
import { RouterLink } from "UI/RouterLink";

import { PAGES } from "constants/navigation";

import { useStyles } from "./styles";

export const MainNav: FC = () => {
  const classes = useStyles();

  return (
    <Grid
      container
      spacing={2}
      component="nav"
      className={classes.nav}
    >
      {PAGES.map(page => (
        <Grid item key={page.label}>
          <RouterLink to={page.path}>
            {page.label}
          </RouterLink>
        </Grid>
      ))}
    </Grid>
  );
};
