import React from "react";
import { useForm, Controller } from "react-hook-form";

import { Box } from "UI/Box";
import { Grid } from "UI/Grid";
import { Button } from "UI/Button";
import { TextField } from "UI/TextField/TextField";

import { addUserApi } from "api/users";

export const NewUserForm = () => {
  const { control, handleSubmit } = useForm();

  const onSubmit = data => addUserApi(data)

  return (
    <Grid container justify="center">
      <Grid item xs sm={8} md={5}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Controller
            name="name"
            control={control}
            defaultValue=""
            render={({
                       field: { onChange, value },
                     }) => (
              <TextField
                label="Name"
                value={value}
                onChange={onChange}
              />
            )}
          />
          <Box mt={1}>
            <Controller
              name="surname"
              control={control}
              defaultValue=""
              render={({
                         field: { onChange, value },
                       }) => (
                <TextField
                  label="Surname"
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </Box>
          <Box mt={1}>
            <Controller
              name="email"
              control={control}
              defaultValue=""
              render={({
                         field: { onChange, value },
                       }) => (
                <TextField
                  label="Email"
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </Box>
          <Box
            mt={2}
            display="flex"
            justifyContent="flex-end"
          >
            <Button type="submit">Create</Button>
          </Box>
        </form>
      </Grid>
    </Grid>
  )
}
