import React, { Suspense } from 'react';
import { Redirect, BrowserRouter, Route, Switch } from "react-router-dom";

import { RouteLoading } from "components/RouteLoading";
import { UsersProvider } from "components/UsersTable/context";

import { PAGES, URL } from "constants/navigation";

export const App = () => (
  <UsersProvider>
    <BrowserRouter>
      <Suspense fallback={<RouteLoading />}>
        <Switch>
          {PAGES.map(({ path, component }) => (
            <Route
              exact
              key={path}
              path={path}
              component={component}
            />
          ))}
          <Redirect to={URL.HOME} />
        </Switch>
      </Suspense>
    </BrowserRouter>
  </UsersProvider>
);
