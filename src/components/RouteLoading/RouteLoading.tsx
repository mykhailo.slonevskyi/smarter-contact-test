import { Grid } from "UI/Grid";
import { Progress } from "UI/Progress";

export const RouteLoading = () => (
  <Grid
    container
    justify="center"
    alignItems="center"
  >
    <Grid item>
      <Progress />
    </Grid>
  </Grid>
);
