import MaterialDialogContentText, { DialogContentTextProps } from "@material-ui/core/DialogContentText";
import React, { FC } from "react";

export const DialogContentText: FC<DialogContentTextProps> = props => (
  <MaterialDialogContentText {...props} />
);
