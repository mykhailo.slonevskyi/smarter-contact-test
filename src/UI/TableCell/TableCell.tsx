import React from "react";
import MaterialTableCell, { TableCellProps } from "@material-ui/core/TableCell";

export const TableCell: React.FC<TableCellProps> = props => (
  <MaterialTableCell {...props} />
);
