import React from "react";
import MaterialList, { ListProps } from "@material-ui/core/List";

export const List: React.FC<ListProps> = React.forwardRef((props, ref) => (
  <MaterialList {...props} ref={ref} />
));
