import MaterialDialogActions, { DialogActionsProps } from "@material-ui/core/DialogActions";
import React, { FC } from "react";

export const DialogActions: FC<DialogActionsProps> = props => (
  <MaterialDialogActions {...props} />
);
