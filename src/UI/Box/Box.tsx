import React from "react";
import MaterialBox, {BoxProps} from "@material-ui/core/Box";

export const Box: React.FC<BoxProps> = props => (
  <MaterialBox {...props} />
)
