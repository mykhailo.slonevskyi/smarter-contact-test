import React from "react";
import CircularProgress, { CircularProgressProps } from "@material-ui/core/CircularProgress";

export const Progress: React.FC<CircularProgressProps> = (props) => (
  <CircularProgress {...props} />
);
