import React from "react";
import MaterialTextField, {TextFieldProps} from "@material-ui/core/TextField";

export const TextField: React.FC<TextFieldProps> = props => (
  <MaterialTextField
    {...props}
  />
);

TextField.defaultProps = {
  fullWidth: true,
};
