import React from "react";
import MaterialIconButton, {IconButtonProps} from "@material-ui/core/IconButton";

export const IconButton: React.FC<IconButtonProps> = props => (
  <MaterialIconButton {...props} />
)

IconButton.defaultProps = {
  color: "primary",
  size: "small"
}
