import MaterialDialog, { DialogProps } from "@material-ui/core/Dialog";
import React, { FC } from "react";

export const Dialog: FC<DialogProps> = props => (
  <MaterialDialog {...props} />
);
