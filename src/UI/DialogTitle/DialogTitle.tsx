import MaterialDialogTitle, { DialogTitleProps } from "@material-ui/core/DialogTitle";
import React, { FC } from "react";

export const DialogTitle: FC<DialogTitleProps> = props => (
  <MaterialDialogTitle {...props} />
);
