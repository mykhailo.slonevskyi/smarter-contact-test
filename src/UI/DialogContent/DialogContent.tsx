import MaterialDialogContent, { DialogContentProps } from "@material-ui/core/DialogContent";
import React, { FC } from "react";

export const DialogContent: FC<DialogContentProps> = props => (
  <MaterialDialogContent {...props} />
);
