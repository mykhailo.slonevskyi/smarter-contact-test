import React from "react";
import MaterialTable, { TableProps } from "@material-ui/core/Table";

export const Table: React.FC<TableProps> = props => (
  <MaterialTable {...props} />
);
