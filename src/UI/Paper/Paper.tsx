import React from "react";
import MaterialPaper, { PaperProps } from "@material-ui/core/Paper";

export const Paper: React.FC<PaperProps> = (props) => (
  <MaterialPaper {...props} />
);
