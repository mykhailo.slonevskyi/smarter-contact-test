import React from "react";
import MaterialTableHead, { TableHeadProps } from "@material-ui/core/TableHead";

export const TableHead: React.FC<TableHeadProps> = props => (
  <MaterialTableHead {...props} />
);
