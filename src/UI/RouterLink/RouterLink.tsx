import React from "react";
import { NavLink, NavLinkProps } from "react-router-dom";

import { useStyles } from "./styles";

export const RouterLink: React.FC<NavLinkProps> = props => {
  const classes = useStyles();

  return (
    <NavLink
      exact
      {...props}
      className={classes.link}
    >
      {props.children}
    </NavLink>
  );
};
