import React from "react";
import MaterialTableBody, { TableBodyProps } from "@material-ui/core/TableBody";

export const TableBody: React.FC<TableBodyProps> = props => (
  <MaterialTableBody {...props} />
);
