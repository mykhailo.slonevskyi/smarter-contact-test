import React from "react";
import MaterialGrid from "@material-ui/core/Grid";

import { IProps } from "./types";

export const Grid: React.FC<IProps> = props => (
  <MaterialGrid {...props} />
);
