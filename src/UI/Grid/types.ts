import { GridProps } from "@material-ui/core/Grid";

export interface IProps extends GridProps {
  component?: React.ElementType<React.HTMLAttributes<HTMLElement>> | React.ComponentType;
}
