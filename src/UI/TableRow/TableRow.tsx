import React from "react";
import MaterialTableRow, { TableRowProps } from "@material-ui/core/TableRow";

export const TableRow: React.FC<TableRowProps> = props => (
  <MaterialTableRow {...props} />
);