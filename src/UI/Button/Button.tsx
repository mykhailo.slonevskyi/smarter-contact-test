import React from "react";
import MaterialButton, {ButtonProps} from "@material-ui/core/Button";

export const Button: React.FC<ButtonProps> = props => (
  <MaterialButton {...props} />
)

Button.defaultProps = {
  color: "primary",
  variant: "contained"
}
