import React from "react";
import MaterialTableContainer, { TableContainerProps } from "@material-ui/core/TableContainer";

import { Paper } from "UI/Paper";

export const TableContainer: React.FC<TableContainerProps> = props => (
  <MaterialTableContainer component={Paper} {...props} />
);
