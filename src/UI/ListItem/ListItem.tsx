import React from "react";
import MaterialListItem, { ListItemProps } from "@material-ui/core/ListItem";

import { IProps } from "./types";

export const ListItem: React.FC<IProps & ListItemProps> = (props) => (
  <MaterialListItem {...props as any} />
);
