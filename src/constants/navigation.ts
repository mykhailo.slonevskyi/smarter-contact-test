import { lazy } from "react";

const HomePage = lazy(() => import("routes/HomePage"));
const NewUserPage = lazy(() => import("routes/NewUserPage"));

export enum URL {
  HOME = "/",
  NEW_USER = "/new-user"
}

export const PAGES = [
  {
    label: "Home",
    path: URL.HOME,
    component: HomePage,
  },
  {
    label: "Create new user",
    component: NewUserPage,
    path: URL.NEW_USER
  }
];