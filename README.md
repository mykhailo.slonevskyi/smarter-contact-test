# SMARTER CONTACT TEST

[![node](https://img.shields.io/badge/node-v15.7.0-green.svg)](https://nodejs.org/en/)
[![yarn](https://img.shields.io/badge/yarn-1.22.5-blue.svg)](https://yarnpkg.com/lang/en/)
___
## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
